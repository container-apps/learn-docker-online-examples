"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
let writeStream = fs.createWriteStream('temp.txt');
class Routes {
    routes(app) {
        app.use((req, res, next) => {
            logRequestData(req, writeStream);
            next();
        }),
            app.route('/')
                .get((req, res) => {
                res.status(200).send({
                    message: 'GET request successfulll!!!!'
                });
            });
    }
}
exports.Routes = Routes;
;
function logRequestData(req, ws) {
    // generate timestamp
    const date = new Date();
    // print out header info
    let printArr = new Array();
    printArr[0] = `IP: ${req.ip}\n`;
    printArr[1] = `Hostname: ${req.hostname} \n`;
    printArr[2] = `Path: ${req.path}\n`;
    printArr[3] = `Timestamp: ${date.toString()}\n`;
    console.log(printArr);
    // log data to file
    let data = '';
    for (let i = 0; i < printArr.length; i++) {
        data += printArr[i];
    }
    if (data.length) {
        ws.write(data);
    }
    else {
        console.log("no data to write to file");
    }
    // the finish event is emitted when all data has been flushed from the stream
    ws.on('finish', () => {
        console.log('wrote all data to file');
    });
}
;
//# sourceMappingURL=routes.js.map